tf-generic-module
=========

Run any given module as a child module.

Note:
> This role does not treat cloud credential separately, user will either has to pass credential via environment variable (assume the provider support it),
> or as TF input variables.

> This role assume the child module is already present in the project path, and will not pulling the module from remote src.

Requirements
------------

This role uses `terraform` ansible module, thus require `terraform` binary to be in PATH.

Role Variables
--------------

|     |     |     |
| --- | --- | --- |
| `TF_PROJECT_PATH` | string | path to the terraform project/workspace where the root module will be generated to, and where the child module should be present |
| `TF_MODULE` | string | source of the child module, relative path to `{{TF_PROJECT_PATH}}/modules/` |
| `TF_INPUT_VALUES` | key-value pair | inputs to the root module, will be passed in the form of `terraform.tfvars` file |
| `TF_INPUT_VARS` | key-value pair | input variable declartion to the child module, with key being name of the variable |
| `TF_OPERATION` | string | the Terraform operation to perform (`apply`, `plan`, `destroy`), default to `apply` |

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Example of role variables:
```yaml
TF_MODULE: your-module
TF_INPUT_VALUES:
    instance_name: "new-instance-1"
TF_INPUT_VARS:
    instance_name:
        type: string
```

Example playbook
```yaml
- hosts: localhost
  roles:
    - tf-generic-module
```

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
